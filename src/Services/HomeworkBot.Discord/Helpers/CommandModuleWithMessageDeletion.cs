﻿using System.Threading.Tasks;
using DSharpPlus.CommandsNext;

namespace HomeworkBot.Discord.Helpers
{
    public class CommandModuleWithMessageDeletion : BaseCommandModule
    {
        public override async Task BeforeExecutionAsync(CommandContext ctx)
        {
            await ctx.Message.DeleteAsync();
        }
    }
}
