﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using Serilog;

namespace HomeworkBot.Discord.Helpers
{
    public static class DiscordMessageExtensions
    {
        private static void DeleteMessageAfterDelay(DiscordMessage message, int delay = 5000)
        {
            var task = new Task(async () =>
            {
                await Task.Delay(delay);
                await message.DeleteAsync();
                Log.Logger.Debug(
                    $"Deleted the message '{message.Content}' of the user '{message.Author.Username}' with the id '{message.Author.Id}'");
            });

            task.Start();
        }

        public static async Task<DiscordMessage> SendMessageAndDeleteAsync(this DiscordClient client, DiscordChannel channel,
            string message = null, bool isTts = false, DiscordEmbed  embed = null, IEnumerable<IMention> mentions = null)
        {
            var result = await client.SendMessageAsync(channel, message, isTts, embed, mentions);
            DeleteMessageAfterDelay(result);
            return result;
        }

        public static async Task<DiscordMessage> SendMessageAndDeleteAsync(this DiscordMember member, string message = null, bool isTts = false, DiscordEmbed embed = null)
        {
            var result = await member.SendMessageAsync(message, isTts, embed);
            DeleteMessageAfterDelay(result, 30000);
            return result;
        }

        public static async Task<DiscordMessage> RespondAndDeleteAsync(this CommandContext context, string message = null, bool isTts = false, DiscordEmbed embed = null, int delay = 5000)
        {
            var result = await context.RespondAsync(message, isTts, embed);
            DeleteMessageAfterDelay(result, delay);
            return result;
        }
    }
}
