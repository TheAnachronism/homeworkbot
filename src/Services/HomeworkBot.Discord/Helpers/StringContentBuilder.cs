﻿using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace HomeworkBot.Discord.Helpers
{
    public static class StringContentBuilder
    {
        public static StringContent CreateStringContent(object content) => new(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json");
    }
}
