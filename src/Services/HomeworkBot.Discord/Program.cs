﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using HomeworkBot.Common.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace HomeworkBot.Discord
{
    public class Program
    {
        public static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        public static async Task MainAsync(string[] args)
        {
            try
            {
                SetupSerilog();
                
                Log.Information("Starting up...");
                var host = CreateHostBuilder(args).Build();

                await host.RunAsync();
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Application startup...");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static void SetupSerilog()
        {
            var loggerConfiguration = new LoggerConfiguration().ConfigureDefaultLogging(Assembly.GetExecutingAssembly());
            Log.Logger = loggerConfiguration.CreateLogger();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            var hostBuilder = Host.CreateDefaultBuilder(args)
                .UseSerilog(Log.Logger)
                .ConfigureLogging((_, builder) =>
                {
                    builder.ClearProviders();
                    builder.AddSerilog(Log.Logger);
                })
                .UseEnvironment(Environment.GetEnvironmentVariable("DOTNET_CORE_ENVIRONMENT"))
                .ConfigureHostConfiguration(builder =>
                {
                    builder.AddEnvironmentVariables().AddJsonFile("appsettings.json");
                })
                .ConfigureServices((hostContext, services) =>
                {
                    var startup = new Startup(hostContext.Configuration, Log.Logger.ForContext<Startup>());
                    startup.ConfigureServices(services);
                })
                .UseConsoleLifetime();

            return hostBuilder;
        }
    }
}
