﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using HomeworkBot.Discord.Helpers;
using HomeworkBot.Shared.Class;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace HomeworkBot.Discord.Commands
{
    [Group("Class")]
    public class ClassCommands : CommandModuleWithMessageDeletion
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<ClassCommands> _logger;

        public ClassCommands(HttpClient httpClient, ILogger<ClassCommands> logger)
        {
            _httpClient = httpClient;
            _logger = logger;
        }

        [Command("enable")]
        public async Task CreateClass(CommandContext context)
        {
            var result = await _httpClient.PostAsync("api/Class/CreateClass",
                StringContentBuilder.CreateStringContent(new CreateClassRequest(context.Channel.Name, context.Guild.Id, context.Channel.Id)));
            if (result.IsSuccessStatusCode)
                await context.RespondAndDeleteAsync($"Added the channel {context.Channel.Mention} as a class. " +
                                                    "Remember, that all users who want to be in this class have to join per command.");
            else
                await context.RespondAndDeleteAsync("There was an error on the API, please contact a moderator.");
        }

        [Command("disable")]
        public async Task RemoveClass(CommandContext context)
        {
            var result = await _httpClient.PostAsync("api/Class/RemoveClass",
                StringContentBuilder.CreateStringContent(new RemoveClassRequest(context.Channel.Id)));
            if (result.IsSuccessStatusCode)
                await context.RespondAndDeleteAsync(
                    $"Removed the channel {context.Channel.Mention} as a class. If you enable it here again, all members have to join again too.");
            else if (result.StatusCode == HttpStatusCode.BadRequest)
            {
                _logger.LogWarning($"The channel {context.Channel.Mention} was requested as a class but isn't registered as one.");
                await context.RespondAndDeleteAsync(
                    $"The channel {context.Channel.Mention} isn't registered as a class.");
            }
            else
                await context.RespondAndDeleteAsync("There was an error on the API, please contact a moderator.");
        }

        [Command("Leave")]
        public async Task LeaveClass(CommandContext context)
        {
            var result = await _httpClient.PostAsync("api/Class/LeaveClass",
                StringContentBuilder.CreateStringContent(new LeaveClassRequest(context.Message.Author.Id, context.Channel.Id)));
            if (result.IsSuccessStatusCode)
                await context.RespondAndDeleteAsync(
                    $"The user {context.Message.Author.Mention} has left the class {context.Channel.Mention}.");
            else if (result.StatusCode == HttpStatusCode.BadRequest)
            {
                _logger.LogWarning($"The channel {context.Channel.Mention} was requested as a class but isn't registered as one.");
                await context.RespondAndDeleteAsync(
                    $"The channel {context.Channel.Mention} is not registered as a class.");
            }
            else
                await context.RespondAndDeleteAsync("There was an error on the API, please contact a moderator.");
        }
        
        [Command("Join")]
        public async Task JoinClass(CommandContext context)
        {
            var result = await _httpClient.PostAsync("api/Class/JoinClass",
                StringContentBuilder.CreateStringContent(new JoinClassRequest(context.Channel.Id, context.Message.Author.Id)));
            if (result.IsSuccessStatusCode)
            {
                var content = JsonConvert.DeserializeObject<JoinClassResponse>(await result.Content.ReadAsStringAsync());
                var channel = await context.Client.GetChannelAsync(content.ClassId);
                await context.RespondAndDeleteAsync(
                    $"The user {context.Message.Author.Mention} to the class {channel.Mention}");
            }
            else if (result.StatusCode == HttpStatusCode.BadRequest)
            {
                _logger.LogWarning($"The channel {context.Channel.Mention} was requested as a class but isn't registered as one.");
                await context.RespondAndDeleteAsync(
                    $"The channel {context.Channel.Mention} is not registered as a class.");
            }
            else
                await context.RespondAndDeleteAsync("There was an error on the API, please contact a moderator.");
        }
    }
}
