﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using HomeworkBot.Discord.Helpers;
using HomeworkBot.Shared.Homework;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace HomeworkBot.Discord.Commands
{
    [Group("Homework")]
    public class HomeworkCommands : CommandModuleWithMessageDeletion
    {
        private readonly ILogger<HomeworkCommands> _logger;
        private readonly HttpClient _httpClient;

        public HomeworkCommands(ILogger<HomeworkCommands> logger, HttpClient httpClient)
        {
            _logger = logger;
            _httpClient = httpClient;
        }

        [Command("Add")]
        public async Task AddHomework(CommandContext context, string subject, string title, string dueDate)
        {
            if (!DateTime.TryParse(dueDate, out var resultDate))
            {
                await context.RespondAndDeleteAsync(
                    "The entered due date could not be parsed, please make sure its in a valid format.");
                return;
            }

            var result = await _httpClient.PostAsync("api/Homework/AddHomework",
                StringContentBuilder.CreateStringContent(new AddHomeworkRequest(context.Channel.Id, subject, title, resultDate)));
            if (result.IsSuccessStatusCode)
            {
                var content =
                    JsonConvert.DeserializeObject<AddHomeworkResponse>(await result.Content.ReadAsStringAsync());
                var users = content.ClassMemberIds.Select(x => context.Guild.Members.Single(y => y.Key == x)).Select(x => x.Value);
                await context.RespondAndDeleteAsync(
                    $"The homework {title} was added which is due to {resultDate.ToShortDateString()}. " +
                    "It will remind the class the day before due.\n" +
                    $"{string.Join(" ", users.Select(x => x.Mention))}", delay: 30000);
            }
            else if (result.StatusCode == HttpStatusCode.BadRequest)
            {
                _logger.LogWarning($"The channel {context.Channel.Mention} was requested as a class but isn't registered as one.");
                await context.RespondAndDeleteAsync(
                    $"The channel {context.Channel.Mention} isn't registered as a class.");
            }
            else
            {
                _logger.LogError($"API response error: {result.ReasonPhrase}");
                await context.RespondAndDeleteAsync("There was an error on the API, please contact a moderator.");
            }
        }

        [Command("Active")]
        public async Task Active(CommandContext context)
        {
            var result = await _httpClient.GetAsync($"api/Homework/Active?id={context.Channel.Id}");
            if (result.IsSuccessStatusCode)
            {
                var content = JsonConvert.DeserializeObject<List<ActiveHomeworkResponse>>(await result.Content.ReadAsStringAsync());
                var embedBuilder = new DiscordEmbedBuilder()
                    .WithTitle("Active Homework")
                    .WithColor(DiscordColor.Blurple);

                foreach (var (subject, description, dateTime) in content)
                {
                    embedBuilder.AddField(subject,
                        $"{description} | {dateTime.ToLocalTime().ToShortDateString()}");
                }

                await context.RespondAndDeleteAsync(embed: embedBuilder.Build(), delay: 15000);
            }
            else if (result.StatusCode == HttpStatusCode.BadRequest)
            {
                _logger.LogWarning($"The channel {context.Channel.Mention} was requested as a class but isn't registered as one.");
                await context.RespondAndDeleteAsync(
                    $"The channel {context.Channel.Mention} isn't registered asa a class.");
            }
            else
            {
                _logger.LogError($"API error: {result.ReasonPhrase}");
                await context.RespondAndDeleteAsync("There was an error on the API, please contact a moderator.");
            }
        }
    }
}
