﻿using System.Net.Http;
using HomeworkBot.Discord.Helpers;
using Microsoft.Extensions.Logging;

namespace HomeworkBot.Discord.Commands
{
    public class BasicCommand : CommandModuleWithMessageDeletion
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<BasicCommand> _logger;

        public BasicCommand(HttpClient httpClient, ILogger<BasicCommand> logger)
        {
            _httpClient = httpClient;
            _logger = logger;
        }
    }
}
