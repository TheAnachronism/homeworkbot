﻿using System;
using System.Net.Http;
using HomeworkBot.Discord.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Steeltoe.Discovery;
using Steeltoe.Discovery.Client;
using Steeltoe.Discovery.Consul;

namespace HomeworkBot.Discord
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;

        private readonly Random _random = new();
        
        public Startup(IConfiguration configuration, ILogger logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddServiceDiscovery(x => x.UseConsul());
            services.AddTransient(sp =>
            {
                var discoveryClient = sp.GetService<IDiscoveryClient>();
                var instances = discoveryClient!.GetInstances("HomeworkBot-API");
                return new HttpClient {BaseAddress = instances[_random.Next(instances.Count - 1)].Uri};
            });
            services.AddSingleton<DiscordBotService>();
            services.AddHostedService(sp => sp.GetService<DiscordBotService>());
            services.AddSingleton<HomeworkReminderService>();
            services.AddHostedService(sp => sp.GetService<HomeworkReminderService>());
        }
    }
}
