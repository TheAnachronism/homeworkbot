﻿using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace HomeworkBot.Discord.Services
{
    public class DiscordBotService : IHostedService
    {
        private readonly ILogger<DiscordBotService> _logger;
        private readonly ILoggerFactory _loggerFactory;
        private readonly IConfiguration _configuration;
        private readonly IServiceProvider _serviceProvider;

        public DiscordShardedClient DiscordShardedClient { get; private set; }
        
        public DiscordBotService(ILogger<DiscordBotService> logger, IConfiguration configuration, ILoggerFactory loggerFactory, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _loggerFactory = loggerFactory;
            _serviceProvider = serviceProvider;
            _configuration = configuration.GetSection("Discord");
        }

        private async Task<DiscordShardedClient> CreateDiscordClient()
        {
            var client = new DiscordShardedClient(new DiscordConfiguration
            {
                Token = _configuration.GetValue<string>("Token"),
                TokenType = TokenType.Bot,
                LoggerFactory = _loggerFactory,
                Intents = DiscordIntents.All
            });

            var commands = await client.UseCommandsNextAsync(new CommandsNextConfiguration
            {
                EnableMentionPrefix = true,
                StringPrefixes = new[] {"!hw"},
                Services = _serviceProvider
            });

            await client.UseInteractivityAsync(new InteractivityConfiguration
            {
                PollBehaviour = PollBehaviour.KeepEmojis,
                Timeout = TimeSpan.FromSeconds(30)
            });

            foreach (var (id, extension) in commands)
            {
                _logger.LogInformation($"Registering commands for sharded client {id}");
                extension.RegisterCommands(Assembly.GetExecutingAssembly());
            }

            return client;
        }
        
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Creating sharded discord client.");
            DiscordShardedClient = await CreateDiscordClient();
            _logger.LogInformation("Starting up discord client service.");
            await DiscordShardedClient.StartAsync();
        }
        
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Shutting down discord client service.");
            await DiscordShardedClient.StopAsync();
        }
    }
}
