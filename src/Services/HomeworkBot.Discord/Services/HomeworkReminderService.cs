﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using DSharpPlus.Entities;
using HomeworkBot.Shared.Homework;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace HomeworkBot.Discord.Services
{
    public class HomeworkReminderService : IHostedService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<HomeworkReminderService> _logger;
        private readonly IHostEnvironment _hostEnvironment;
        
        private Timer _timer;
        
        public HomeworkReminderService(ILogger<HomeworkReminderService> logger, IHostEnvironment hostEnvironment, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _hostEnvironment = hostEnvironment;
            _serviceProvider = serviceProvider;
        }

        public async Task RemindHomework()
        {
            var httpClient = _serviceProvider.GetService<HttpClient>();
            var shardedClient = _serviceProvider.GetService<DiscordBotService>()!.DiscordShardedClient;
            
            var result = await httpClient!.GetAsync("api/Homework/DueHomework");
            if (!result.IsSuccessStatusCode)
            {
                _logger.LogError($"API error: {result.ReasonPhrase}");
                return;
            }

            var stringContent = await result.Content.ReadAsStringAsync();
            var content = JsonConvert.DeserializeObject<DueTomorrowResponse>(stringContent);

            var sendTasks = new List<Task>();
            
            foreach (var (guildId, channelId, classMembers, dueTomorrowHomeworkResponses) in content.Classes)
            {
                var shard = shardedClient!.GetShard(guildId);
                var guild = await shard.GetGuildAsync(guildId);
                var channel = guild.GetChannel(channelId);
                
                var embed = new DiscordEmbedBuilder().WithDescription($"**You have to following homework in {channel.Mention}.**")
                    .WithColor(DiscordColor.Blurple);

                foreach (var subjectGroup in dueTomorrowHomeworkResponses.GroupBy(x => x.Subject))
                    embed.AddField(subjectGroup.Key,
                        string.Join("\n",
                            subjectGroup.Select(x => $"{x.Description} | {x.DueDate.ToShortDateString()}")));
                
                var memberTasks = classMembers.Select(x => guild.GetMemberAsync(x)).ToList();
                await Task.WhenAll(memberTasks);

                var embedBuild = embed.Build();
                sendTasks.AddRange(memberTasks.Select(x => x.Result)
                    .Select(member => member.SendMessageAsync(embed: embedBuild)));
            }

            await Task.WhenAll(sendTasks);
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = _hostEnvironment.IsDevelopment() ? TimeSpan.FromMinutes(2) : TimeSpan.FromDays(1);

            _timer = new Timer(async (_) =>
            {
                await RemindHomework();
            }, null, startTimeSpan, periodTimeSpan);
            return Task.CompletedTask;
        }
        
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _timer.DisposeAsync();
        }
    }
}
