﻿using System;

namespace HomeworkBot.API.Data
{
    public class DbHomework
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public Guid DbClassId { get; set; }
        public virtual DbClass DbClass { get; set; }
    }
}
