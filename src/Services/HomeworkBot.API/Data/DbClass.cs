﻿using System;
using System.Collections.Generic;

namespace HomeworkBot.API.Data
{
    public class DbClass
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ulong DiscordGuildId { get; set; }
        public ulong DiscordChannelId { get; set; }
        public virtual List<DbClassMember> Members { get; set; }
        public virtual List<DbHomework> Homework { get; set; }
    }
}
