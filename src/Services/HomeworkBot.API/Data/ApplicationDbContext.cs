﻿using Microsoft.EntityFrameworkCore;

namespace HomeworkBot.API.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<DbClass> Classes { get; set; }
        public DbSet<DbClassMember> ClassMembers { get; set; }
        public DbSet<DbHomework> Homework { get; set; }
    }
}
