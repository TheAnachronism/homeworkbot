﻿using System;
using System.Collections.Generic;

namespace HomeworkBot.API.Data
{
    public class DbClassMember
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public ulong DiscordId { get; set; }
        public virtual List<DbClass> DbClasses { get; set; }
    }
}
