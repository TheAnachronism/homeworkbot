﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using HomeworkBot.API.Data;
using HomeworkBot.Shared.Homework;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HomeworkBot.API.Controllers
{
    [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
    public class HomeworkController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public HomeworkController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ActiveHomeworkResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Active(ulong id)
        {
            var dbClass = await _context.Classes.SingleOrDefaultAsync(x => x.DiscordChannelId == id);
            if (dbClass == null)
                return BadRequest();

            var result = await _context.Homework.Include(x => x.DbClass)
                .Where(x => x.DbClassId == dbClass.Id && x.DueDate > DateTime.UtcNow)
                .ToListAsync();

            return Ok(result.Select(x => new ActiveHomeworkResponse(x.Subject, x.Description, x.DueDate)));
        }

        [HttpPost]
        [ProducesResponseType(typeof(AddHomeworkResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AddHomework([FromBody] AddHomeworkRequest request)
        {
            var dbClass = await _context.Classes.SingleOrDefaultAsync(x => x.DiscordChannelId == request.ClassId);
            if (dbClass == null)
                return BadRequest();

            var homework = new DbHomework
            {
                DbClass = dbClass,
                Description = request.Title,
                DueDate = request.DueDate.ToUniversalTime(),
                Subject = request.Subject
            };

            var result = await _context.Homework.AddAsync(homework);
            await _context.SaveChangesAsync();

            var users = await _context.ClassMembers
                .Where(x => x.DbClasses.Any(y => y.DiscordChannelId == dbClass.DiscordChannelId))
                .Select(x => x.DiscordId).ToListAsync();
            return Ok(new AddHomeworkResponse(users));
        }

        [HttpGet]
        [ProducesResponseType(typeof(DueTomorrowResponse), StatusCodes.Status200OK)]
        public async Task<IActionResult> DueHomework()
        {
            var classes = await _context.Classes
                .Include(x => x.Members)
                .Include(x => x.Homework.Where(y => y.DueDate > DateTime.UtcNow))
                .ToListAsync();

            var classResults = (from dbClass in classes
                let homeworkResults = dbClass.Homework.Select(x => new DueTomorrowHomeworkResponse(x.Subject,
                    x.Description,
                    x.DueDate))
                select new DueTomorrowClassResponse(dbClass.DiscordGuildId, dbClass.DiscordChannelId,
                    dbClass.Members.Select(x => x.DiscordId),
                    homeworkResults)).ToList();

            return Ok(new DueTomorrowResponse(classResults));
        }
    }
}
