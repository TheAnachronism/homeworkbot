﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeworkBot.API.Data;
using HomeworkBot.Shared.Class;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Steeltoe.Connector.Services;

namespace HomeworkBot.API.Controllers
{
    [ProducesResponseType(typeof(Exception), StatusCodes.Status500InternalServerError)]
    public class ClassController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ClassController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateClass([FromBody] CreateClassRequest request)
        {
            var (className, classGuild, classChannel) = request;
            if (await _context.Classes.AnyAsync(x => x.DiscordChannelId == request.ClassChannelId))
                return Ok();
            
            var newClass = new DbClass
            {
                Name = className,
                DiscordChannelId = classChannel,
                DiscordGuildId = classGuild
            };

            await _context.Classes.AddAsync(newClass);
            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> RemoveClass([FromBody] RemoveClassRequest request)
        {
            var discordClass =
                await _context.Classes.SingleOrDefaultAsync(x => x.DiscordChannelId == request.DiscordChannelId);
            if (discordClass == null)
                return BadRequest();

            _context.Classes.Remove(discordClass);
            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> LeaveClass([FromBody] LeaveClassRequest request)
        {
            var discordClass =
                await _context.Classes
                    .Include(x => x.Members.Where(x => x.DiscordId == request.DiscordMemberId))
                    .SingleOrDefaultAsync(x => x.DiscordChannelId == request.DiscordChannelId);
            if (discordClass == null)
                return BadRequest();

            var discordMember =
                await _context.ClassMembers.SingleOrDefaultAsync(x => x.DiscordId == request.DiscordMemberId);
            if (discordMember == null)
                return Ok();

            discordClass.Members.Remove(discordMember);
            _context.Update(discordClass);
            await _context.SaveChangesAsync();
            return Ok();
        }
        
        [HttpPost]
        [ProducesResponseType(typeof(JoinClassResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> JoinClass([FromBody] JoinClassRequest request)
        {
            var discordClass = await _context.Classes.SingleAsync(x => x.DiscordChannelId == request.DiscordChannelId);
            if (discordClass == null)
                return BadRequest();
            
            var member = await _context.ClassMembers.SingleOrDefaultAsync(x => x.DiscordId == request.DiscordMemberId);
            if (member != null)
            {
                if (member.DbClasses.Any(x => x.DiscordChannelId == request.DiscordChannelId))
                    return Ok(new JoinClassResponse(discordClass.DiscordChannelId));
                
                member.DbClasses.Add(discordClass);
                await _context.SaveChangesAsync();
                return Ok(new JoinClassResponse(discordClass.DiscordChannelId));
            }

            member = new DbClassMember
            {
                DiscordId = request.DiscordMemberId,
                DbClasses = new List<DbClass> { discordClass}
            };

            member = (await _context.ClassMembers.AddAsync(member)).Entity;
            await _context.SaveChangesAsync();

            return Ok(new JoinClassResponse(discordClass.DiscordChannelId));
        }
    }
}
