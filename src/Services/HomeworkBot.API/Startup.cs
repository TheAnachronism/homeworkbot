using HomeworkBot.API.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using Serilog;
using Steeltoe.Discovery.Client;
using Steeltoe.Discovery.Consul;
using Swashbuckle.AspNetCore.SwaggerGen.ConventionalRouting;

namespace HomeworkBot.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseLazyLoadingProxies();
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"),
                    b =>
                    {
                        b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName);
                        b.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                    });
            });
            
            services.AddControllers().AddNewtonsoftJson(opts => opts.SerializerSettings.Converters.Add(new StringEnumConverter()));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "HomeworkBot.API", 
                    Version = "v1",
                    Description = "API for the HomeworkBot"
                });
                c.EnableAnnotations();
            });

            services.AddSwaggerGenNewtonsoftSupport();
            services.AddSwaggerGenWithConventionalRoutes();
            
            services.AddLogging(builder => builder.AddSerilog(dispose: true));

            services.AddServiceDiscovery(x => x.UseConsul());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger(options =>
                {
                    options.RouteTemplate = "docs/{documentName}/docs/json";
                });
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/docs/v1/docs.json", "HomeworkBot.API v1");
                    c.RoutePrefix = "docs";
                });
            }
            
            app.UseRouting();

            app.UseAuthorization();

            app.UseDiscoveryClient();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "api/{controller}/{action}/{id?}");
                ConventionalRoutingSwaggerGen.UseRoutes(endpoints);
            });
        }
    }
}
