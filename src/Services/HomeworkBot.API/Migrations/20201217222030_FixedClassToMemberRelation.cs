﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HomeworkBot.API.Migrations
{
    public partial class FixedClassToMemberRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassMembers_Classes_DbClassId",
                table: "ClassMembers");

            migrationBuilder.DropIndex(
                name: "IX_ClassMembers_DbClassId",
                table: "ClassMembers");

            migrationBuilder.DropColumn(
                name: "DbClassId",
                table: "ClassMembers");

            migrationBuilder.AddColumn<decimal>(
                name: "DiscordChannelId",
                table: "Classes",
                type: "numeric(20,0)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscordGuildId",
                table: "Classes",
                type: "numeric(20,0)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "DbClassDbClassMember",
                columns: table => new
                {
                    DbClassesId = table.Column<Guid>(type: "uuid", nullable: false),
                    MembersId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbClassDbClassMember", x => new { x.DbClassesId, x.MembersId });
                    table.ForeignKey(
                        name: "FK_DbClassDbClassMember_Classes_DbClassesId",
                        column: x => x.DbClassesId,
                        principalTable: "Classes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DbClassDbClassMember_ClassMembers_MembersId",
                        column: x => x.MembersId,
                        principalTable: "ClassMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DbClassDbClassMember_MembersId",
                table: "DbClassDbClassMember",
                column: "MembersId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DbClassDbClassMember");

            migrationBuilder.DropColumn(
                name: "DiscordChannelId",
                table: "Classes");

            migrationBuilder.DropColumn(
                name: "DiscordGuildId",
                table: "Classes");

            migrationBuilder.AddColumn<Guid>(
                name: "DbClassId",
                table: "ClassMembers",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_ClassMembers_DbClassId",
                table: "ClassMembers",
                column: "DbClassId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassMembers_Classes_DbClassId",
                table: "ClassMembers",
                column: "DbClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
