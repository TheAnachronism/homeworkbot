using System;
using System.Reflection;
using System.Threading.Tasks;
using HomeworkBot.API.Data;
using HomeworkBot.Common.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace HomeworkBot.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        public static async Task MainAsync(string[] args)
        {
            try
            {
                SetupSerilog();
                
                Log.Information("Starting up...");
                var apiHost = CreateHostBuilder(args).Build();

                var config = apiHost.Services.GetRequiredService<IConfiguration>();
                if (config.GetValue<bool>("ASPNET_CORE_APPLY_MIGRATIONS_ON_STARTUP"))
                    await apiHost.MigrateDatabase<ApplicationDbContext, Program>(apiHost.Services.GetService<ILogger<Program>>());

                await apiHost.RunAsync();
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Application startup failed.");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static void SetupSerilog()
        {
            var loggerConfiguration =
                new LoggerConfiguration().ConfigureDefaultLogging(Assembly.GetExecutingAssembly());

            Log.Logger = loggerConfiguration.CreateLogger();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog(Log.Logger)
                .ConfigureLogging((context, builder) =>
                {
                    builder.ClearProviders();
                    builder.AddSerilog(Log.Logger);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
