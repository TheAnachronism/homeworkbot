﻿using System.Reflection;
using Serilog;
using Serilog.Events;

namespace HomeworkBot.Common.Extensions
{
    public static class LoggerExtensions
    {
        public static LoggerConfiguration ConfigureDefaultLogging(this LoggerConfiguration loggerConfiguration, Assembly service)
        {
            loggerConfiguration.MinimumLevel.Debug();
            loggerConfiguration
                .MinimumLevel.Override("Default", LogEventLevel.Information)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information);

            loggerConfiguration.Enrich.FromLogContext().Enrich.WithMachineName();
            
            loggerConfiguration.WriteTo.Logger(logger =>
                {
                    logger.WriteTo.Console(outputTemplate: "{Timestamp:o} [{Level:u3}] ({SourceContext}) {Message}{NewLine}{Exception}");
                })
                .WriteTo.Logger(logger =>
                {
                    logger.Filter.ByIncludingOnly("(@l = 'Error' or @l = 'Fatal' or @l = 'Warning')");
                    logger.WriteTo.File(
                        $"/logs/{service.GetName().Name}/exceptions_.log",
                        outputTemplate:
                        "{Timestamp:o} [{Level:u3}] ({SourceContext}) {Message}{NewLine}{Exception}",
                        rollingInterval: RollingInterval.Day,
                        retainedFileCountLimit: 7);
                })
                .WriteTo.Logger(logger =>
                {
                    logger.Filter.ByIncludingOnly("(@l = 'Information' or @l = 'Debug')");
                    logger.WriteTo.File(
                        $"/logs/{service.GetName().Name}/information_.log",
                        outputTemplate:
                        "{Timestamp:o} [{Level:u3}] ({SourceContext}) {Message}{NewLine}{Exception}",
                        rollingInterval: RollingInterval.Day,
                        retainedFileCountLimit: 7);
                });

            return loggerConfiguration;
        }
    }
}
