﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace HomeworkBot.Common.Extensions
{
    public static class HostExtensions
    {
        public static async Task<IHost> MigrateDatabase<TContext, TAssembly>(this IHost webHost,
            ILogger<TAssembly> logger)
            where TContext : DbContext
        {
            var serviceScopeFactory = webHost.Services.GetService<IServiceScopeFactory>();

            using var scope = serviceScopeFactory.CreateScope();
            var services = scope.ServiceProvider;
            var dbContext = services.GetRequiredService<TContext>();

            var pendingMigrations = (await dbContext.Database.GetPendingMigrationsAsync()).ToList();
            if (pendingMigrations.Any())
            {
                logger.LogWarning($"Trying to apply {pendingMigrations.Count} pending migrations.");
                await dbContext.Database.MigrateAsync();
                logger.LogWarning($"Successfully applied {pendingMigrations.Count} pending database migrations.");
            }

            return webHost;
        }
    }
}
