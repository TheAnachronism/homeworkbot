﻿using System;

namespace HomeworkBot.Shared.Homework
{
    public record ActiveHomeworkResponse(string Subject, string Description, DateTime DueDate);
}
