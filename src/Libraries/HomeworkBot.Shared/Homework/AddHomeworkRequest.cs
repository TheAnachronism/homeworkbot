﻿using System;

namespace HomeworkBot.Shared.Homework
{
    public record AddHomeworkRequest(ulong ClassId, string Subject, string Title, DateTime DueDate);
}
