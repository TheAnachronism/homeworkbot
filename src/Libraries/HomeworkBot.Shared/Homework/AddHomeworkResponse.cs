﻿using System.Collections.Generic;

namespace HomeworkBot.Shared.Homework
{
    public record AddHomeworkResponse(IEnumerable<ulong> ClassMemberIds);
}
