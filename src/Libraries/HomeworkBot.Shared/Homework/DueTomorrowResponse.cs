﻿using System;
using System.Collections.Generic;

namespace HomeworkBot.Shared.Homework
{
    public record DueTomorrowClassResponse(ulong GuildId, ulong ChannelId, IEnumerable<ulong> ClassMembers, IEnumerable<DueTomorrowHomeworkResponse> Homework);

    public record DueTomorrowHomeworkResponse(string Subject, string Description, DateTime DueDate);

    public record DueTomorrowResponse(IEnumerable<DueTomorrowClassResponse> Classes);
}