﻿namespace HomeworkBot.Shared.Class
{
    public record RemoveClassRequest(ulong DiscordChannelId);
}
