﻿namespace HomeworkBot.Shared.Class
{
    public record CreateClassRequest(string ClassName, ulong ClassGuildId, ulong ClassChannelId);
}
