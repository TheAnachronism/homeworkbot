﻿namespace HomeworkBot.Shared.Class
{
    public record JoinClassResponse(ulong ClassId);
}
