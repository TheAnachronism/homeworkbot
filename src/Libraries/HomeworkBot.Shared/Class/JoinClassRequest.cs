﻿namespace HomeworkBot.Shared.Class
{
    public record JoinClassRequest(ulong DiscordChannelId, ulong DiscordMemberId);
}
