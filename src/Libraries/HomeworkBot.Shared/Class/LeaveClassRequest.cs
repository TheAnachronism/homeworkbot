﻿namespace HomeworkBot.Shared.Class
{
    public record LeaveClassRequest(ulong DiscordMemberId, ulong DiscordChannelId);
}
